package sondow.tetra.conf;

import java.util.ArrayList;
import java.util.List;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class BotConfigFactory {

    public BotConfig configure() {
        Configuration readingTwitterConf = configureTwitterRole(TwitterAccountRole.READING);
        Configuration writingTwitterConf = configureTwitterRole(TwitterAccountRole.WRITING);
        Integer turnMinutes = Environment.getInt("TURN_LENGTH_MINUTES");
        if (turnMinutes == null) {
            turnMinutes = 20;
        } else if (turnMinutes < 5) {
            throw new RuntimeException("Twitter requires a minimum of 5 minutes for a poll but " +
                    "the TURN_LENGTH_MINUTES environment variable is set to " + turnMinutes);
        }
        String airtableApiKey = Environment.require("CRED_AIRTABLE_API_KEY");
        String airtableBaseId = Environment.require("CRED_AIRTABLE_BASE");

        List<Configuration> publicityConfigs = new ArrayList<>();
        String publicityAccounts = Environment.get("PUBLICITY_ACCOUNTS");
        if (publicityAccounts != null) {
            String[] accounts = publicityAccounts.split(",");
            for (String account : accounts) {
                Configuration twitterConfig = configureTwitter(account, TrimUser.DISABLED);
                publicityConfigs.add(twitterConfig);
            }
        }

        return new BotConfig(readingTwitterConf, writingTwitterConf, turnMinutes, airtableApiKey,
                airtableBaseId, publicityConfigs);
    }

    /**
     * AWS Lambda only allows underscores in environment variables, not dots, so the default ways
     * twitter4j finds keys aren't possible. Instead, this custom code gets the configuration from
     * Lambda-friendly environment variables. Also, because of quirks of Twitter's poll API we often
     * need two sets of credentials, one for reading and one for writing.
     *
     * @param role the role of the account credentials we want, either reading or writing
     * @return configuration containing Twitter authentication strings and other variables
     */
    private Configuration configureTwitterRole(TwitterAccountRole role) {

        String roleName = role.getRoleName();

        return configureTwitter(roleName, TrimUser.ENABLED);
    }

    private Configuration configureTwitter(String prefix, TrimUser trimUser) {
        ConfigurationBuilder configBuilder = new ConfigurationBuilder();

        String credentialsCsv = Environment.require(prefix + "_twitter_credentials");

        String[] tokens = credentialsCsv.split(",");
        String screenName = tokens[0];
        String consumerKey = tokens[1];
        String consumerSecret = tokens[2];
        String accessToken = tokens[3];
        String accessTokenSecret = tokens[4];

        configBuilder.setUser(screenName);
        configBuilder.setOAuthConsumerKey(consumerKey);
        configBuilder.setOAuthConsumerSecret(consumerSecret);
        configBuilder.setOAuthAccessToken(accessToken);
        configBuilder.setOAuthAccessTokenSecret(accessTokenSecret);

        boolean trimDecision = trimUser.getValue();
        return configBuilder.setTrimUserEnabled(trimDecision).setTweetModeExtended(true).build();
    }
}
