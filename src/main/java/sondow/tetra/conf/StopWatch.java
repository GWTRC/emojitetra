package sondow.tetra.conf;

import java.time.Duration;
import java.time.Instant;
import org.apache.log4j.Logger;

public class StopWatch {

    private final static Logger log = Logger.getLogger(StopWatch.class);
    private String name;
    private String activity;
    private Instant start;
    private Instant stop;

    private StopWatch(String name, String activity) {
        this.name = name;
        this.activity = activity;
    }

    public static StopWatch start(String name, String activity) {
        StopWatch stopWatch = new StopWatch(name, activity);
        stopWatch.startActivity(activity);
        return stopWatch;
    }

    public void startActivity(String activity) {
        this.activity = activity;
        stop = null;
        start = Instant.now();
    }

    public Duration reportAndRestart(String activity) {
        Duration duration = stopAndReport();
        startActivity(activity);
        return duration;
    }

    public Duration stopAndReport() {
        stop = Instant.now();
        Duration duration = Duration.between(start, stop);
        long seconds = duration.getSeconds();
        log.info(name + " " + activity + " took " + (seconds < 1 ? "<1s" : seconds + "s"));
        return duration;
    }
}




