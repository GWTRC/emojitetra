package sondow.tetra.conf;

import java.time.ZonedDateTime;
import org.apache.log4j.Logger;

public class Time {

    private final static Logger log = Logger.getLogger(Time.class);

    public ZonedDateTime nowZonedDateTime() {
        return ZonedDateTime.now();
    }

    public void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    public void waitASec() {
        sleep(1000L);
    }
}
