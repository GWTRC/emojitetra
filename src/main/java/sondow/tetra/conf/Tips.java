package sondow.tetra.conf;

import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * To get these messages out regularly but without much repetition, the message of the day and the
 * time that message gets tweeted are selected by an algorithm based on the current date.
 * <p>
 * If the current date of the month has a corresponding message, that message is the message of the
 * day.
 * <p>
 * If the current date of the month does not have a corresponding message, subtract the total number
 * of messages from the current date. The result is the key for the message of the day.
 */
public class Tips {

    /**
     * Yes, technically this would be more elegant as a list, but I deliberately made it a map of
     * dates to strings so that I would remember that the ordered placement of each message to its
     * corresponding date number is very important, to avoid repetitions of similar messages near
     * each other, and to remind future me that adding a new message somewhere in the collection
     * should require some thought, considering what got tweeted in the past few days.
     */
    private Map<Integer, TweetContent> datesToMessages = new HashMap<>();

    public Tips() {

        put(1, "This game is maintained by @JoeSondow who also tries to make Twitter less of a " +
                "nightmare. This de-politicized fork was made by @RealRedCap, and I'm still adjusting" + 
                " some of the tips. I've left Joe's patreon links, he did make the bot after all.");
        put(2, "Game tip\n\nBefore you vote, check the side of the tweet to see what shape the " +
                "Next piece is. Think about how you want to use both the current and the next " +
                "piece together.");
        put(3, "(Placeholder for now, I gotta put in the Search Link)");
        put(4, "Game tip\n\nClearing multiple lines in one move earns more points than clearing " +
                "the same number of lines one at a time.");
        put(5, "Game tip\n\nIf there are comments below one of my tweets, read the comments (yes " +
                "really) for strategy ideas before you vote.");
        put(6, "This is the infamous tip, where everyone gets blocked. Nobody gets blocked on this account." + 
                " If you want to use \"fellas\" or \"guys,\" I'm not stoppin ya.");
        put(7, "If you want to drop some money in the tip jar for the developer of this game, " +
                "feel free. Here are his links: paypal.me/JoeSondow or ko-fi.com/JoeSondow");
        put(8, "Game tip\n\nThe most common order of moves for a given piece is rotate, sideways " +
                "movement, then down and plummet. If you're in agreement with others about the " +
                "piece's destination, this order is your best bet to avoid vote splitting.");
        put(9, "Game tip\n\nThis game is one of those rare places on the internet where it’s " +
                "helpful to read the comments. Some players draw diagrams showing clever " +
                "potential uses of the current and next pieces.");
        put(10, "Game tip\n\nThis game grid is smaller than what you’re probably used to. Try to " +
                "keep the top four rows clear enough to be able to rotate a 1x4 piece.");
        put(11, "Game tip\n\nWhen choosing where to drop a piece, consider what \"silhouette\" or" +
                " \"skyline\" will result at the bottom. The safest situation is one where there " +
                "is a good place to put any of the possible shapes.");
        put(12, "\"haha I can block whoever!\" Not on this bot you can't. Eat your heart out Joe :)");
        put(13, "This game is and always will be free, but if you want to tip the developer, " +
                "you can do so at patreon.com/JoeSondow. He maintains the bot, but not this fork.");
        put(14, "Game tip\n\nThis game is all text. You can copy, paste, and edit the game board " +
                "in a reply to illustrate to other players what strategy you recommend.");
        put(15, "Game tip\n\nIt's safe to cover a hole if the next piece will uncover that hole " +
                "in an obvious way.");
        put(16, "When drawing your recommendation with emojis, avoid 1️⃣ and 2️⃣ because they " +
                "don’t render properly on some devices. Try one of these emoji pairs " +
                "instead:\n\n🥇🥈\n🔴🔵");
        put(17, "FAQ: https://gitlab.com/GWTRC/emojitetra/blob/master/README.md");
        put(18, "This was a tip about \"toxic behavior.\" You're not children. I'm not gonna pull a \"ya'll can't behave.\"" );
        put(19, "This game is the product of hundreds of hours of work, with plenty more still to" +
                "do. I may have de-politicized the bot, but Joe still developed it. Drop him a tip here: " +
                "patreon.com/JoeSondow");
        put(20, "Game tip\n\nThe all-time high score is in my account bio. It's seperate from the official" + 
                "@EmojiTetra");
        put(21, "How Scoring Works\n\nSingle line cleared +100 points\n2 lines in one move +250 " +
                "points\n3 lines in one move +525 points\n4 lines in one move +1000 " +
                "points\n\nFull board clear +1000 points\n\nhttps://gitlab" +
                ".com/GWTRC/emojitetra/blob/master/README.md#how-does-the-score-board-work");
        put(22, new TweetContent("Here's a gif showing the rotation rules for the shapes that " +
                "rotate.", new FileClerk().getFile("emojitetrarotations.gif")));
        put(23, "Game Tip\n\nThe current shape choosing algorithm is completely random. The " +
                "shapes that came recently have no bearing on which shapes to expect soon.");
        put(24, "Game Tip\n\nThe game usually ends as a result of a combination of greed for " +
                "points, hope for a 1x4 piece, and bad luck. Go for risky fancy 4-line clears " +
                "near the bottom, not the top.");
        put(25, "This tip was the \"you get blocked\" song. That doesn't work here, now does it?");
        put(26, "You can help support this game by retweeting its game polls to help grow the " +
                "player base, especially since this is such a small fork.");
        put(27, "Game tip\n\nIf you need to build towers to avoid burying holes, try to build " +
                "them against the far left or right side, not in the middle.");
        put(28, "Game tip\n\nKeep your eye on the \"Next\" indicator on the side of each game " +
                "tweet.");
        put(29, "Game tip\n\nSometimes it's possible to put a piece where it looks as if it will " +
                "cover a gap but instead it will instantly vanish" +
                ".\n\n◽🍕🍕🍕🍕◽◽\n◽◽◽◽◽◽🍕\n🍔◽◽◽◽🌮🍕\n🍔🍔◽◽◽🌮🍕\n🍔🍟🍟◽◽🌮🍕");
        put(30, "Game tip\n\nLonger games score higher. If you're patient and your goal is a high" +
                " score, play it safe. If your goal is more frequent bursts of dopamine and the " +
                "high score is less important to you, go for the high risk, high reward tactics.");
        put(31, "Game tip\n\nThere is currently no gravity in this game. Pieces descend only when" +
                " Down or Plummet wins a poll. Rotations and lateral movements are unlimited.");
    }

    private void put(int index, String s) {
        put(index, new TweetContent(s, null));
    }

    private void put(int index, TweetContent t) {
        datesToMessages.put(index, t);
    }

    public TweetContent getMessageForDayOfMonth(int dayOfMonth) {
        assert dayOfMonth >= 1;
        TweetContent tweetContent = datesToMessages.get(dayOfMonth);
        if (tweetContent == null) {
            int messageCount = datesToMessages.size();
            tweetContent = datesToMessages.get(dayOfMonth % messageCount);
        }
        return tweetContent;
    }

    /**
     * Peak time is 7am to 3pm Pacific Time, which is 2pm to 10pm UTC, or 14 to 22 UTC military
     * time. For a target time between 14 and 22 inclusive, which span 9 hours, mod the date by 9
     * and add 14.
     *
     * @param dayOfMonth the date of the month, e.g. 12 for the 12th day of the current month
     * @return the 0-24 hour of the day at which to tweet the message of the day
     */
    int getHourFor(int dayOfMonth) {
        return (dayOfMonth % 9) + 14;
    }

    /**
     * Checks whether it's time to tweet a tip message based on the date of the month, the hour of
     * the day, and whether the current time is within the first x minutes of the hour, where x is
     * the number of minutes between game tweets.
     *
     * @param dateTime        the current date time
     * @param intervalMinutes the number of minutes between game tweets
     * @return true iff it's time for a tip tweet
     */
    public boolean isTimeForTipMessage(ZonedDateTime dateTime, int intervalMinutes) {
        int dayOfMonth = dateTime.getDayOfMonth();
        int hour = dateTime.getHour();
        return getHourFor(dayOfMonth) == hour && dateTime.getMinute() < intervalMinutes;
    }

    List<String> getAllMessages() {
        Collection<TweetContent> values = datesToMessages.values();
        return values.stream().map(TweetContent::getMessage).collect(Collectors.toList());
    }
}
