package sondow.tetra.io;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.game.Move;
import sondow.tetra.game.Tile;
import sondow.tetra.shape.Shape;
import sondow.tetra.shape.ShapeType;
import twitter4j.JSONException;
import twitter4j.JSONObject;

/**
 * Converts stuff into other stuff, mainly json to tetra game and back.
 *
 * @author @JoeSondow
 */
public class Converter {

    private Random random;

    public Converter(Random random) {
        this.random = random;
    }

    private Pattern pattern = Pattern.compile("" +
                    "([-*.0-9A-Z]+)" + // Ascii rows
                    "([a-z])" + // Emoji set
                    "([0-9]+)" + // Score
                    "([A-Z])" + // Next shape
                    "([0-9a-z]+)" + // Tweet ID compressed to base 36
                    "([A-Z])" + // Direction
                    "([0-9]+)" + // Row
                    "([A-Z])" + // Current shape
                    "([0-9]+)" + // Column
                    "([a-z])" + // Previous move
                    "([0-9]+)" // Thread counter
            // "9*.7-.7-.3L.3n300Z109158062592W1J6d37"
    );

    public Game makeGameFromCompressedString(String compressed) {
        Game game;
        Matcher matcher = pattern.matcher(compressed);
        if (matcher.matches()) {
            String compressedRows = matcher.group(1);
            String emojiSetCode = matcher.group(2);
            String scoreString = matcher.group(3);
            String nextShapeCode = matcher.group(4);
            String idBase36 = matcher.group(5);
            String directionLetter = matcher.group(6);
            String rowIndex = matcher.group(7);
            String currentShapeLetter = matcher.group(8);
            String colIndex = matcher.group(9);
            String prevMoveCode = matcher.group(10);
            String threadLengthString = matcher.group(11);

            EmojiSet emojiSet = EmojiSet.fromCode(emojiSetCode);
            game = new Game(emojiSet, random);
            parseCompressedRowsAndAddTilesToGame(compressedRows, game);
            int curRow = Integer.parseInt(rowIndex);
            int curCol = Integer.parseInt(colIndex);
            Move prev = prevMoveCode.equals(Move.NULL_CODE) ? null : Move.fromCode(prevMoveCode);
            Direction direction = Direction.fromInitial(directionLetter);
            ShapeType type = ShapeType.fromLetter(currentShapeLetter);
            Shape shape = ShapeType.createShape(type, game, direction, curRow, curCol, prev);
            int score = Integer.parseInt(scoreString);
            Long tweetId = "0".equals(idBase36) ? null : expandFromBase36(idBase36);
            ShapeType nextShapeType = ShapeType.fromLetter(nextShapeCode);
            int threadLength = Integer.parseInt(threadLengthString);
            game.setPiece(shape).setScore(score).setNextShape(nextShapeType).setTweetId(tweetId);
            game.setThreadLength(threadLength);
        } else {
            throw new RuntimeException(compressed + " does not match regex " + pattern.pattern());
        }
        return game;
    }

    public Game makeGameFromJson(String json) {
        try {
            return parseJson(json);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private Game parseJson(String json) throws JSONException {
        JSONObject obj = new JSONObject(json);
        String themeName = obj.getString("thm");
        EmojiSet emojiSet = EmojiSet.valueOf(themeName);
        Game game = new Game(emojiSet, random);
        String compressedRows = obj.getString("rows");
        parseCompressedRowsAndAddTilesToGame(compressedRows, game);
        JSONObject currentPieceJson = obj.getJSONObject("cur");
        String currentTypeLetter = currentPieceJson.getString("t");
        int curCol = currentPieceJson.getInt("c");
        int curRow = currentPieceJson.getInt("r");
        String directionLetter = currentPieceJson.getString("d");
        Move prevMove = null;
        if (currentPieceJson.has("p")) {
            String previousMoveAbbr = currentPieceJson.getString("p");
            prevMove = Move.fromAbbreviation(previousMoveAbbr);
        }

        Direction direction = Direction.fromInitial(directionLetter);
        ShapeType type = ShapeType.fromLetter(currentTypeLetter);
        Shape shape = ShapeType.createShape(type, game, direction, curRow, curCol, prevMove);
        int score = obj.getInt("sc");
        long tweetId = obj.getLong("id");
        String nextLatinChar = obj.getString("nx");
        ShapeType nextShapeType = ShapeType.fromLetter(nextLatinChar);
        int threadLength = obj.getInt("thr");
        game.setPiece(shape).setScore(score).setNextShape(nextShapeType).setTweetId(tweetId);
        game.setThreadLength(threadLength);

        return game;
    }

    private void parseCompressedRowsAndAddTilesToGame(String compressedRows, Game game) {
        List<String> expandedEncodedRows = expandEncodedRows(compressedRows);

        for (int r = 0; r < expandedEncodedRows.size(); r++) {
            String rowLatin = expandedEncodedRows.get(r);
            // Iterate through the characters of the string. Skip anything that isn't a known shape.
            for (int c = 0; c < rowLatin.length(); c++) {
                String typeLetter = "" + rowLatin.charAt(c);
                if (ShapeType.isValidLetter(typeLetter)) {
                    ShapeType shapeType = ShapeType.fromLetter("" + typeLetter);
                    Tile tile = new Tile(shapeType, game);
                    tile.setCol(c);
                    tile.setRow(r);
                    game.addTile(tile, r, c);
                }
            }
        }
    }

    List<String> expandEncodedRows(String compressedRowsHyphenDelimited) {
        String[] compressedRowStrings = compressedRowsHyphenDelimited.split("-");
        List<String> latinRows = new ArrayList<>();
        for (String compressedSegment : compressedRowStrings) {
            char first = compressedSegment.charAt(0);
            char second = compressedSegment.charAt(1);
            String encodedRow;
            int times = 1;
            if (Character.isDigit(first) && second == '*') {
                times = Character.getNumericValue(first);
                encodedRow = compressedSegment.substring(2);
            } else {
                encodedRow = compressedSegment;
            }
            String expanded = expandRow(compressedRowsHyphenDelimited, encodedRow);
            for (int t = 0; t < times; t++) {
                latinRows.add(expanded);
            }
        }
        return latinRows;
    }

    private String expandRow(String compressedRowsHyphenDelimited, String compressedSegment) {
        // Iterate through the characters of the string.
        // If you encounter a digit, use that many copies of the previous character.
        StringBuilder builder = new StringBuilder();
        for (int c = 0; c < compressedSegment.length(); c++) {
            char ch = compressedSegment.charAt(c);
            if (Character.isDigit(ch)) {
                int count = Character.getNumericValue(ch);
                failIfDigitIsFirst(c, compressedSegment, compressedRowsHyphenDelimited);
                char previous = compressedSegment.charAt(c - 1);
                for (int i = 0; i < count - 1; i++) {
                    builder.append(previous);
                }
            } else {
                builder.append(ch);
            }
        }
        return builder.toString();
    }

    private void failIfDigitIsFirst(int c, String compressedRow, String compressedRowsHyphenDelim) {
        if (c <= 0) {
            ParseException parseException = new ParseException("Digit found at start " +
                    "of compressed row " + compressedRow + " in game data " +
                    compressedRowsHyphenDelim, 0);
            throw new RuntimeException(parseException);
        }
    }

    public String makeCompressedStringFromGame(Game game) {
        StringBuilder builder = new StringBuilder();
        Shape piece = game.getPiece();
        Move previousMove = piece.getPreviousMove();
        String prevMoveCode = previousMove == null ? Move.NULL_CODE : previousMove.getCode();
        Long tweetId = game.getTweetId();
        if (tweetId == null) {
            tweetId = 0L;
        }
        String tweetIdCompressed = compressToBase36(tweetId);
        builder.append(getCompressedAsciiRows(game)).append(game.getEmojiSet().code).
                append(game.getScore()).append(game.getNextShape().getCode()).
                append(tweetIdCompressed).append(piece.getDirection().code).
                append(piece.getRowIndex()).append(piece.getShapeType().getCode()).
                append(piece.getColumnIndex()).append(prevMoveCode).append(game.getThreadLength());
        return builder.toString();
    }

    public String makeJsonFromGame(Game game) {

        Map<String, Object> currentPieceMap = new HashMap<>();
        Shape current = game.getPiece();
        String letter = current.getShapeType().getCode();
        currentPieceMap.put("t", letter);
        currentPieceMap.put("c", current.getColumnIndex());
        currentPieceMap.put("r", current.getRowIndex());
        currentPieceMap.put("d", current.getDirection().code);
        Move previousMove = current.getPreviousMove();
        if (previousMove != null) {
            currentPieceMap.put("p", previousMove.getAbbreviation());
        }

        String compressedRows = getCompressedAsciiRows(game);
        Map<String, Object> gameMap = new HashMap<>();
        gameMap.put("thm", game.getEmojiSet().name());
        gameMap.put("rows", compressedRows);
        gameMap.put("cur", currentPieceMap);
        gameMap.put("nx", game.getNextShape().getCode());
        gameMap.put("sc", game.getScore());
        gameMap.put("thr", game.getThreadLength());
        gameMap.put("id", game.getTweetId());

        return new JSONObject(gameMap).toString();
    }

    private String getCompressedAsciiRows(Game game) {
        List<List<Tile>> rows = game.getRows();
        List<String> asciiRows = new ArrayList<>();
        for (List<Tile> row : rows) {
            StringBuilder asciiRow = new StringBuilder();
            for (Tile tile : row) {
                String representation;
                if (tile == null) {
                    representation = ".";
                } else {
                    representation = tile.getShapeType().getCode();
                }
                asciiRow.append(representation);
            }
            asciiRows.add(asciiRow.toString());
        }
        return compressAsciiRows(asciiRows);
    }

    String compressAsciiRows(List<String> asciiRows) {
        List<String> compressedRows = new ArrayList<>();
        for (String asciiRow : asciiRows) {
            StringBuilder rowBuilder = new StringBuilder();
            // Iterate through the characters of the row. If a character matches the one before it,
            // increment the number of consecutive times that character should be included. When you
            // find a different character (or reach 9) add the character summary to the rowBuilder.
            // ....... becomes .7
            // ...LLL. becomes .3L3.
            // ..TTTLL becomes ..T3LL
            // ........... becomes .9..
            int times = 1;
            Character prev = null;
            for (int c = 0; c < asciiRow.length(); c++) {
                char cur = asciiRow.charAt(c);
                if (c == 0) {
                    rowBuilder.append(cur);
                } else {
                    prev = asciiRow.charAt(c - 1);
                    if (prev == cur && times < 9) {
                        times++;
                    } else {
                        appendFinalCharacterForStreak(rowBuilder, times, prev);
                        rowBuilder.append(cur);
                        times = 1;
                    }
                }
            }
            if (prev != null) {
                appendFinalCharacterForStreak(rowBuilder, times, prev);
            }
            compressedRows.add(rowBuilder.toString());
        }
        // Compress identical consecutive rows.
        // .7
        // .7
        // .7
        // ..TTLL.
        //
        // becomes
        //
        // 3*.7
        // ..TTLL.
        List<String> stage2 = new ArrayList<>();
        int times = 1;
        String curRow = null;
        for (int k = 0; k < compressedRows.size(); k++) {
            curRow = compressedRows.get(k);
            if (k >= 1) {
                String prevRow = compressedRows.get(k - 1);
                if (prevRow.equals(curRow) && times < 9) {
                    times++;
                } else {
                    stage2.add(times >= 2 ? times + "*" + prevRow : prevRow);
                    times = 1;
                }
            }
        }
        if (curRow != null) {
            stage2.add(times >= 2 ? times + "*" + curRow : curRow);
        }
        return String.join("-", stage2);
    }

    private void appendFinalCharacterForStreak(StringBuilder rowBuilder, int times, char prev) {
        if (times == 2) {
            rowBuilder.append(prev);
        } else if (times > 2) {
            rowBuilder.append(times);
        }
    }

    public String compressToBase36(Long bigNumber) {
        // Tweet ID can be shrunk from 19 to 12 characters.
        // 85evejsj9n28 1072764856921518080
        // The result is mixed alphanumeric but only lowercase,
        // so only use capital letters for other values and this'll work fine.
        BigInteger bigInteger = new BigInteger("" + bigNumber);
        return bigInteger.toString(Character.MAX_RADIX);
    }

    public Long expandFromBase36(String compressed) {
        BigInteger uncompressed = new BigInteger(compressed, Character.MAX_RADIX);
        return uncompressed.longValue();
    }
}
