package sondow.tetra.io;

public interface Database {

    String readGameState();

    void writeGameState(String state);

    void deleteGameState();
}
