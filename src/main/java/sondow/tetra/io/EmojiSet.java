package sondow.tetra.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.log4j.Logger;
import sondow.tetra.shape.ShapeType;
import static sondow.tetra.io.EmojiSetType.COMMON;
import static sondow.tetra.io.EmojiSetType.HOLIDAY;

public enum EmojiSet {

    ANIMAL("n", "🐾", "🐷", "🐯", "🐵", "🐱", "🐻", "🐨", "🐼", COMMON),
    AQUATIC("q", "🐚", "🐙", "🐡", "🐠", "🐬", "🐟", "🐳", "🐌", COMMON),
    BOOK("b", "📖", "📕", "📙", "📒", "📗", "📘", "📓", "📚", COMMON),
    CHRISTMAS("s", "🎶", "🎅", "🎁", "🔔", "🎄", "❄", "🍪", "🦌", HOLIDAY),
    CLOTHING("o", "👙", "👚", "👘", "👔", "👛", "👒", "🎒", "🎩", COMMON),
    FRUIT("f", "💩", "🍎", "🍑", "🍋", "🍏", "🍇", "🍉", "🍓", COMMON),
    HALLOWEEN("e", "💀", "👺", "🎃", "😱", "🍭", "🔮", "😈", "🍫", HOLIDAY),
    HEART("r", "💋", "💘", "💖", "💛", "💚", "💙", "💜", "💗", COMMON),
    PLANT("p", "🍂", "🌸", "🌻", "🌹", "🍀", "🌿", "🌺", "🍄", COMMON);

    final String code;
    final String gone;
    final String zee;
    final String line;
    final String tee;
    final String jay;
    final String square;
    final String ell;
    final String ess;
    final EmojiSetType type;

    private final static Logger log = Logger.getLogger(EmojiSet.class);

    EmojiSet(String code, String gone, String zee, String line, String tee, String jay,
            String square, String ell, String ess, EmojiSetType type) {
        this.code = code;
        this.gone = gone;
        this.zee = zee;
        this.line = line;
        this.tee = tee;
        this.jay = jay;
        this.square = square;
        this.ell = ell;
        this.ess = ess;
        this.type = type;
    }

    /**
     * This medium white square looks better on twitter than it does in the IntelliJ editor.
     * <p>
     * Alternatives could include a large white square ⬜ but I don't like that as much, or a small
     * white square ▫️ but that looks even worse in IntelliJ and used to take up 4 Twitter
     * characters instead of 2 Twitter characters, so it was a technical impossibility. I might
     * consider switching to that (or a black square) later.
     * <p>
     * Best choice I've found is the medium white square ◽ which renders in IntelliJ only if it's
     * followed by some other weird character ◽️ or if you pick a font like Monaco that display ◽
     * correctly, as opposed to Menlo which does not.
     */
    public static final String blank = "◽";

    public String getGone() {
        return gone;
    }

    public String showcase() {
        return zee + line + tee + jay + square + ell + ess;
    }

    public static EmojiSet basedOnShowcase(String showcase) {
        EmojiSet[] allSets = values();
        EmojiSet choice = null;
        for (EmojiSet emojiSet : allSets) {
            if (showcase.equals(emojiSet.showcase())) {
                choice = emojiSet;
                break;
            }
        }
        return choice;
    }

    public static List<EmojiSet> pickAny(int count, Random random) {
        List<EmojiSet> selection = new ArrayList<>();
        List<EmojiSet> common = common();
        for (int i = 0; i < count; i++) {
            while (selection.size() < count) {
                int choiceIndex = random.nextInt(common.size());
                EmojiSet emojiSet = common.get(choiceIndex);
                if (!selection.contains(emojiSet)) {
                    selection.add(emojiSet);
                }
            }
        }
        return selection;
    }

    public String symbolFor(ShapeType shapeType) {
        String symbol = null;
        if (shapeType.equals(ShapeType.ZEE)) {
            symbol = zee;
        } else if (shapeType.equals(ShapeType.LINE)) {
            symbol = line;
        } else if (shapeType.equals(ShapeType.TEE)) {
            symbol = tee;
        } else if (shapeType.equals(ShapeType.JAY)) {
            symbol = jay;
        } else if (shapeType.equals(ShapeType.SQUARE)) {
            symbol = square;
        } else if (shapeType.equals(ShapeType.ELL)) {
            symbol = ell;
        } else if (shapeType.equals(ShapeType.ESS)) {
            symbol = ess;
        }
        return symbol;
    }

    public static EmojiSet pickOne(Random random) {
        List<EmojiSet> sets = common();
        return sets.get(random.nextInt(sets.size()));
    }

    public static List<EmojiSet> common() {
        EmojiSet[] values = EmojiSet.values();
        List<EmojiSet> common = new ArrayList<>();
        for (EmojiSet emojiSet : values) {
            if (emojiSet.type == COMMON) {
                common.add(emojiSet);
            }
        }
        return common;
    }

    public static EmojiSet fromCode(String code) {
        EmojiSet[] values = EmojiSet.values();
        List<EmojiSet> matches = new ArrayList<>();
        for (EmojiSet emojiSet : values) {
            if (emojiSet.code.equals(code)) {
                matches.add(emojiSet);
            }
        }
        assert matches.size() == 1;
        return matches.get(0);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 20; i++) {
            log.info(EmojiSet.pickAny(4, new Random()));
        }

        String aquaticShowcase = EmojiSet.AQUATIC.showcase();
        log.info(aquaticShowcase);
        log.info(EmojiSet.basedOnShowcase(aquaticShowcase) == EmojiSet.AQUATIC);
    }
}
