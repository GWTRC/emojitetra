package sondow.tetra.shape;

import sondow.tetra.game.Direction;
import sondow.tetra.game.Game;
import sondow.tetra.game.Move;

public class ShapeConfig {

    private final Game context;
    private final Direction direction;
    private final int rowIndex;
    private final int columnIndex;
    private final Move previousMove;

    ShapeConfig(Game context, Direction direction, int rowIndex, int colIndex, Move prevMove) {
        this.context = context;
        this.direction = direction;
        this.rowIndex = rowIndex;
        this.columnIndex = colIndex;
        this.previousMove = prevMove;
    }

    Game getContext() {
        return context;
    }

    public Direction getDirection() {
        return direction;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    int getColumnIndex() {
        return columnIndex;
    }

    Move getPreviousMove() {
        return previousMove;
    }
}
