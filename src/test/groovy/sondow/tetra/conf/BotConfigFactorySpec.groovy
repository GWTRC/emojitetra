package sondow.tetra.conf

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification
import twitter4j.conf.Configuration

class BotConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration with environment variables"() {
        setup:
        String filler = Environment.SPACE_FILLER
        envVars.set("reading_twitter_credentials",
                "${filler}disney,mickey,donald,goofy,pluto${filler}")
        envVars.set("writing_twitter_credentials",
                "${filler}${filler}looneytunes,bugs,daffy,porky,sylvester${filler}")
        envVars.set("CRED_AIRTABLE_API_KEY",
                "${filler}${filler}${filler}chewy${filler}${filler}${filler}")
        envVars.set("CRED_AIRTABLE_BASE",
                "${filler}${filler}${filler}han${filler}${filler}${filler}")

        when:
        Configuration writingConfig = new BotConfigFactory().configure().getWritingTwitterConfig()
        Configuration readingConfig = new BotConfigFactory().configure().getReadingTwitterConfig()

        then:
        with(readingConfig) {
            user == 'disney'
            OAuthConsumerKey == 'mickey'
            OAuthConsumerSecret == 'donald'
            OAuthAccessToken == 'goofy'
            OAuthAccessTokenSecret == 'pluto'
        }
        with(writingConfig) {
            user == 'looneytunes'
            OAuthConsumerKey == 'bugs'
            OAuthConsumerSecret == 'daffy'
            OAuthAccessToken == 'porky'
            OAuthAccessTokenSecret == 'sylvester'
        }
    }
}
