package sondow.tetra.game

import spock.lang.Specification
import static sondow.tetra.game.Move.DOWN
import static sondow.tetra.game.Move.LEFT
import static sondow.tetra.game.Move.LEFT_OR_RIGHT
import static sondow.tetra.game.Move.PLUMMET
import static sondow.tetra.game.Move.RIGHT
import static sondow.tetra.game.Move.ROTATE
import static sondow.tetra.game.Move.STOP

class MoveSpec extends Specification {

    def "should get Move enumeration items from database codes"() {
        expect:
        LEFT == Move.fromCode("f")
        LEFT_OR_RIGHT == Move.fromCode("o")
        RIGHT == Move.fromCode("i")
        ROTATE == Move.fromCode("a")
        DOWN == Move.fromCode("d")
        STOP == Move.fromCode("s")
        PLUMMET == Move.fromCode("p")
    }

    def "should get Move enumeration items from database abbreviations"() {
        expect:
        LEFT == Move.fromAbbreviation("lf")
        LEFT_OR_RIGHT == Move.fromAbbreviation("lr")
        RIGHT == Move.fromAbbreviation("ri")
        ROTATE == Move.fromAbbreviation("ro")
        DOWN == Move.fromAbbreviation("dn")
        STOP == Move.fromAbbreviation("st")
        PLUMMET == Move.fromAbbreviation("pl")
    }

    def "should get Move enumeration items from poll entry strings"() {
        expect:
        LEFT == Move.fromEmojiAndTitleCase("⬅️ Left")
        LEFT_OR_RIGHT == Move.fromEmojiAndTitleCase("↔️ Left or Right")
        RIGHT == Move.fromEmojiAndTitleCase("➡️ Right")
        ROTATE == Move.fromEmojiAndTitleCase("🔄 Rotate")
        DOWN == Move.fromEmojiAndTitleCase("⬇️ Down")
        STOP == Move.fromEmojiAndTitleCase("⬇️ Stop")
        PLUMMET == Move.fromEmojiAndTitleCase("⏬ Plummet")
    }

    def "should make poll choices from moves and preserve order"() {
        when:
        String left = "⬅️ Left"
        String right = "➡️ Right"
        String leftOrRight = "↔️ Left or Right"
        String rotate = "🔄 Rotate"
        String down = "⬇️ Down"
        String stop = "⬇️ Stop"
        String plummet = "⏬ Plummet"

        then:
        [left] == Move.toPollChoices([LEFT])
        [right] == Move.toPollChoices([RIGHT])
        [leftOrRight] == Move.toPollChoices([LEFT_OR_RIGHT])
        [rotate] == Move.toPollChoices([ROTATE])
        [down] == Move.toPollChoices([DOWN])
        [stop] == Move.toPollChoices([STOP])
        [plummet] == Move.toPollChoices([PLUMMET])
        [left, right, rotate, down, stop] == Move.toPollChoices([LEFT, RIGHT, ROTATE, DOWN, STOP])
        [right, rotate, down] == Move.toPollChoices([RIGHT, ROTATE, DOWN])
        [left, stop] == Move.toPollChoices([LEFT, STOP])
    }
}
