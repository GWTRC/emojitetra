package sondow.tetra.io

import sondow.tetra.game.Game
import sondow.tetra.game.Tile
import sondow.tetra.shape.ShapeType
import spock.lang.Specification
import static sondow.tetra.game.Direction.EAST
import static sondow.tetra.game.Direction.NORTH
import static sondow.tetra.game.Direction.SOUTH
import static sondow.tetra.game.Direction.WEST
import static sondow.tetra.game.Move.DOWN
import static sondow.tetra.game.Move.LEFT
import static sondow.tetra.game.Move.LEFT_OR_RIGHT
import static sondow.tetra.game.Move.PLUMMET
import static sondow.tetra.game.Move.RIGHT
import static sondow.tetra.game.Move.ROTATE
import static sondow.tetra.game.Move.STOP
import static sondow.tetra.io.EmojiSet.ANIMAL
import static sondow.tetra.io.EmojiSet.AQUATIC
import static sondow.tetra.io.EmojiSet.BOOK
import static sondow.tetra.io.EmojiSet.CHRISTMAS
import static sondow.tetra.io.EmojiSet.CLOTHING
import static sondow.tetra.io.EmojiSet.FRUIT
import static sondow.tetra.io.EmojiSet.HALLOWEEN
import static sondow.tetra.io.EmojiSet.HEART
import static sondow.tetra.io.EmojiSet.PLANT
import static sondow.tetra.shape.ShapeType.ELL
import static sondow.tetra.shape.ShapeType.ESS
import static sondow.tetra.shape.ShapeType.JAY
import static sondow.tetra.shape.ShapeType.LINE
import static sondow.tetra.shape.ShapeType.SQUARE
import static sondow.tetra.shape.ShapeType.TEE
import static sondow.tetra.shape.ShapeType.ZEE

class ConverterSpec extends Specification {

    def 'should convert compressed string to game'() {

        setup:
        String contents = '9*.7-.7-.I4ZZf100Z8ak6ausypz0gE2J3d31'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🍏◽◽◽　Next\n" +
                "◽◽◽🍏◽◽◽　◽🍎\n" +
                "◽◽🍏🍏◽◽◽　🍎🍎\n" +
                "◽◽◽◽◽◽◽　🍎\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　100\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽🍑🍑🍑🍑🍎🍎"

        when:
        Game game = converter.makeGameFromCompressedString(contents)

        then:
        game.threadLength == 31
        game.tweetId == 1091584191628062592L
        game.emojiSet == FRUIT
        game.toString() == expected
        game.score == 100
        game.piece.shapeType == JAY
        game.piece.rowIndex == 2
        game.piece.columnIndex == 3
        game.piece.direction == EAST
        game.piece.previousMove == DOWN
        game.nextShape == ZEE
    }

    def 'should convert compressed string to game with more cases'() {

        setup:
        String contents = compressed
        Converter converter = new Converter(new Random(4))

        when:
        Game game = converter.makeGameFromCompressedString(contents)

        then:
        game.threadLength == th
        game.tweetId == id
        game.emojiSet == thm
        game.score == sc
        game.nextShape == nx
        game.piece.shapeType == LINE
        game.piece.rowIndex == r
        game.piece.columnIndex == c
        game.piece.direction == dir
        game.piece.previousMove == prev

        where:
        th | id     | thm     | sc   | nx   | r  | c | dir   | prev   | compressed
        31 | 82694L | FRUIT   | 100  | ZEE  | 2  | 3 | EAST  | DOWN   | '9*.7-2*.7f100Z1rt2E2I3d31'
        0  | 295L   | ANIMAL  | 250  | ELL  | 4  | 5 | WEST  | LEFT   | '9*.7-2*.7n250L87W4I5f0'
        50 | null   | AQUATIC | 3400 | ESS  | 1  | 2 | NORTH | RIGHT  | '9*.7-2*.7q3400S0N1I2i50'
        50 | 150L   | AQUATIC | 0    | LINE | 10 | 4 | SOUTH | ROTATE | '9*.7-2*.7q0I46S10I4a50'
        13 | 11996L | AQUATIC | 0    | LINE | 0  | 4 | SOUTH | ROTATE | '2*.7-9*.J6q0I998S0I4a13'
    }

    def 'should convert json formatted game string to game with and without previous move'() {

        setup:
        String contents = '{"thm":"FRUIT","thmLen":57,"thr":31,"rows":"9*.7-.7-.I4ZZ","cur":' +
                '{"t":"J","c":3,"r":2,"d":"E"' + prevDb + '},"nx":"Z","sc":100,"id":1234567890}'
        Converter converter = new Converter(new Random(4))
        String expected = "" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽🍏◽◽◽　Next\n" +
                "◽◽◽🍏◽◽◽　◽🍎\n" +
                "◽◽🍏🍏◽◽◽　🍎🍎\n" +
                "◽◽◽◽◽◽◽　🍎\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽◽◽◽◽◽◽　Score\n" +
                "◽◽◽◽◽◽◽　100\n" +
                "◽◽◽◽◽◽◽\n" +
                "◽🍑🍑🍑🍑🍎🍎"

        when:
        Game game = converter.makeGameFromJson(contents)

        then:
        game.threadLength == 31
        game.toString() == expected
        game.score == 100
        game.piece.previousMove == prevMove
        game.nextShape == ZEE

        where:
        prevDb      | prevMove
        ''          | null
        ',"p":"lf"' | LEFT
        ',"p":"ri"' | RIGHT
        ',"p":"lr"' | LEFT_OR_RIGHT
        ',"p":"ro"' | ROTATE
        ',"p":"dn"' | DOWN
        ',"p":"st"' | STOP
        ',"p":"pl"' | PLUMMET
    }

    def 'should convert simple game to database compressed v2 string'() {

        setup:
        Random random = new Random(2)
        Converter converter = new Converter(random)
        Game game = new Game(ANIMAL, random)
        game.piece = ShapeType.createShape(JAY, game, WEST, 1, 3, DOWN)
        game.nextShape = ZEE
        game.score = 300
        game.threadLength = 37
        game.tweetId = 1091584191628062592L
        game.addTile(new Tile(ELL, game), 10, 3)

        when:
        String string = converter.makeCompressedStringFromGame(game)

        then:
        string == '9*.7-.7-.3L.3n300Z8ak6ausypz0gW1J3d37'
    }

    def 'should convert simple game to database compressed v2 string with some options part 1'() {

        setup:
        Random random = new Random(2)
        Converter converter = new Converter(random)
        Game game = new Game(thm, random)
        game.piece = ShapeType.createShape(JAY, game, WEST, r, c, prev)
        game.nextShape = nx
        game.score = sc
        game.threadLength = th
        game.tweetId = 109158062592L
        game.addTile(new Tile(ELL, game), 10, 3)

        when:
        String string = converter.makeCompressedStringFromGame(game)

        then:
        string == result

        where:
        result                                | thm       | sc    | nx     | r | c | prev   | th
        '9*.7-.7-.3L.3n300Z1e59w45cW1J6d37'   | ANIMAL    | 300   | ZEE    | 1 | 6 | DOWN   | 37
        '9*.7-.7-.3L.3q0I1e59w45cW2J5f37'     | AQUATIC   | 0     | LINE   | 2 | 5 | LEFT   | 37
        '9*.7-.7-.3L.3b45050T1e59w45cW3J4i37' | BOOK      | 45050 | TEE    | 3 | 4 | RIGHT  | 37
        '9*.7-.7-.3L.3o1200J1e59w45cW4J3s37'  | CLOTHING  | 1200  | JAY    | 4 | 3 | STOP   | 37
        '9*.7-.7-.3L.3s300O1e59w45cW5J1n37'   | CHRISTMAS | 300   | SQUARE | 5 | 1 | null   | 37
        '9*.7-.7-.3L.3f12400S1e59w45cW6J3a0'  | FRUIT     | 12400 | ESS    | 6 | 3 | ROTATE | 0
        '9*.7-.7-.3L.3e300Z1e59w45cW7J2d37'   | HALLOWEEN | 300   | ZEE    | 7 | 2 | DOWN   | 37
        '9*.7-.7-.3L.3r300Z1e59w45cW8J1d37'   | HEART     | 300   | ZEE    | 8 | 1 | DOWN   | 37
        '9*.7-.7-.3L.3p300Z1e59w45cW9J1d2'    | PLANT     | 300   | ZEE    | 9 | 1 | DOWN   | 2
    }

    def 'should convert simple game to database compressed v2 string with some options part 2'() {

        setup:
        Random random = new Random(2)
        Converter converter = new Converter(random)
        Game game = new Game(ANIMAL, random)
        game.piece = ShapeType.createShape(sh, game, dir, 2, 5, prev)
        game.nextShape = JAY
        game.score = 300
        game.threadLength = 29
        game.tweetId = id
        game.addTile(new Tile(ELL, game), 10, 3)

        when:
        String string = converter.makeCompressedStringFromGame(game)

        then:
        string == result

        where:
        result                              | id            | sh     | dir   | prev
        '9*.7-.7-.3L.3n300J1e59w45cW2Z5d29' | 109158062592L | ZEE    | WEST  | DOWN
        '9*.7-.7-.3L.3n300J96tv89E2I5f29'   | 555666777L    | LINE   | EAST  | LEFT
        '9*.7-.7-.3L.3n300Jd8kavN2T5i29'    | 22234567L     | TEE    | NORTH | RIGHT
        '9*.7-.7-.3L.3n300J0S2J5s29'        | null          | JAY    | SOUTH | STOP
        '9*.7-.7-.3L.3n300J1e59w45cW2O5n29' | 109158062592L | SQUARE | WEST  | null
        '9*.7-.7-.3L.3n300J1e59w45cW2L5a29' | 109158062592L | ELL    | WEST  | ROTATE
        '9*.7-.7-.3L.3n300J1e59w45cW2S5o29' | 109158062592L | ESS    | WEST  | LEFT_OR_RIGHT
        '9*.7-.7-.3L.3n300J1e59w45cW2J5p29' | 109158062592L | JAY    | WEST  | PLUMMET
        '9*.7-.7-.3L.3n300J1e59w45cW2J5d29' | 109158062592L | JAY    | WEST  | DOWN
    }

    def 'should convert simple game to database json string with and without previous move'() {

        setup:
        Random random = new Random(2)
        Converter converter = new Converter(random)
        Game game = new Game(ANIMAL, random)
        game.piece = ShapeType.createShape(JAY, game, WEST, 1, 3, prevMove)
        game.nextShape = SQUARE
        game.score = 300
        game.threadLength = 24
        game.tweetId = 656565L
        game.addTile(new Tile(ELL, game), 10, 3)

        when:
        String json = converter.makeJsonFromGame(game)

        then:
        json == '{"sc":300,"cur":{' + prevDb + '"r":1,"c":3,"t":"J","d":"W"},"thm":"ANIMAL",' +
                '"nx":"O","id":656565,"rows":"9*.7-.7-.3L.3","thr":24}'

        where:
        prevMove      | prevDb
        null          | ''
        LEFT          | '"p":"lf",'
        RIGHT         | '"p":"ri",'
        LEFT_OR_RIGHT | '"p":"lr",'
        ROTATE        | '"p":"ro",'
        DOWN          | '"p":"dn",'
        PLUMMET       | '"p":"pl",'
        STOP          | '"p":"st",'
    }

    def 'compress and expand big numbers like tweet ids in base 36'() {
        setup:
        Converter converter = new Converter()

        when:
        long id = 1092340931066441728L
        String compressed = converter.compressToBase36(id)
        long expanded = converter.expandFromBase36(compressed)

        then:
        compressed == '8armjk3pwd8g'
        expanded == id
    }

    def 'try to expand big numbers that are not compressed in base 36'() {
        setup:
        Converter converter = new Converter()

        when:
        long id = 0L
        long expanded = converter.expandFromBase36("" + id)

        then:
        expanded == id
    }
}
