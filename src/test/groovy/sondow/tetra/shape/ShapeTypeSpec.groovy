package sondow.tetra.shape

import sondow.tetra.game.Direction
import sondow.tetra.game.Game
import sondow.tetra.io.EmojiSet
import spock.lang.Specification

class ShapeTypeSpec extends Specification {

    def 'should get shape type from letter'() {
        when:
        String exceptionMessage = null
        Throwable t
        try {
            ShapeType.fromLetter("P")
        } catch (NoSuchShapeException e) {
            t = e
            exceptionMessage = t.getMessage()
        }
        then:
        exceptionMessage == "No ShapeType for P"
        ShapeType.fromLetter("Z") == ShapeType.ZEE
        ShapeType.fromLetter("I") == ShapeType.LINE
        ShapeType.fromLetter("T") == ShapeType.TEE
        ShapeType.fromLetter("J") == ShapeType.JAY
        ShapeType.fromLetter("O") == ShapeType.SQUARE
        ShapeType.fromLetter("L") == ShapeType.ELL
        ShapeType.fromLetter("S") == ShapeType.ESS
        t != null
    }

    def 'should create shape'() {
        when:
        Game game = new Game(EmojiSet.BOOK, new Random(2))
        Shape shape1 = ShapeType.createShapeAtSpawnPoint(ShapeType.ZEE, game, Direction.NORTH)
        Shape shape2 = ShapeType.createShapeAtSpawnPoint(ShapeType.LINE, game, Direction.NORTH)
        Shape shape3 = ShapeType.createShapeAtSpawnPoint(ShapeType.TEE, game, Direction.NORTH)

        then:
        shape1.getClass() == ShapeZee
        shape1.context == game
        shape2.getClass() == ShapeLine
        shape2.context == game
        shape3.getClass() == ShapeTee
        shape3.context == game
    }

    def 'createAny should create random shapes'() {
        when:
        Random random = new Random(5L)
        Game game = new Game(EmojiSet.BOOK, new Random(2))
        Shape shape1 = ShapeType.createAny(random, game)
        Shape shape2 = ShapeType.createAny(random, game)
        Shape shape3 = ShapeType.createAny(random, game)
        Shape shape4 = ShapeType.createAny(random, game)

        then:
        shape1.getClass() == ShapeTee
        shape2.getClass() == ShapeSquare
        shape3.getClass() == ShapeSquare
        shape4.getClass() == ShapeZee
    }

    def 'should check which letters are valid shape types'() {
        expect:
        ShapeType.isValidLetter("Z")
        ShapeType.isValidLetter("I")
        ShapeType.isValidLetter("T")
        ShapeType.isValidLetter("J")
        ShapeType.isValidLetter("O")
        ShapeType.isValidLetter("L")
        ShapeType.isValidLetter("S")
        !ShapeType.isValidLetter(".")
        !ShapeType.isValidLetter("z")
        !ShapeType.isValidLetter("ZZ")
        !ShapeType.isValidLetter("..")
    }

    def 'should get bottom tile row index'() {
        when:
        Game game = new Game(EmojiSet.BOOK, new Random(2))
        Shape shapeZ = ShapeType.createShapeAtSpawnPoint(ShapeType.ZEE, game, Direction.NORTH)
        Shape shapeI = ShapeType.createShapeAtSpawnPoint(ShapeType.LINE, game, Direction.NORTH)
        Shape shapeT = ShapeType.createShapeAtSpawnPoint(ShapeType.TEE, game, Direction.NORTH)

        then:
        shapeZ.getBottomTileRowIndex() == 1
        shapeI.getBottomTileRowIndex() == 0
        shapeT.getBottomTileRowIndex() == 1
    }

    def 'should get tall display formats'() {
        when:
        List<String> zeeDisplay = ShapeType.ZEE.getTallDisplay('👕')
        List<String> lineDisplay = ShapeType.LINE.getTallDisplay('💙')
        List<String> teeDisplay = ShapeType.TEE.getTallDisplay('🍊')
        List<String> jayDisplay = ShapeType.JAY.getTallDisplay('🍄')
        List<String> squareDisplay = ShapeType.SQUARE.getTallDisplay('🐙')
        List<String> ellDisplay = ShapeType.ELL.getTallDisplay('🐱')
        List<String> essDisplay = ShapeType.ESS.getTallDisplay('🍫')

        then:
        zeeDisplay == [
                '◽👕',
                '👕👕',
                '👕'
        ]
        lineDisplay == [
                '💙',
                '💙',
                '💙',
                '💙'
        ]
        teeDisplay == [
                '🍊',
                '🍊🍊',
                '🍊'
        ]
        jayDisplay == [
                '🍄🍄',
                '🍄',
                '🍄'
        ]
        squareDisplay == [
                '🐙🐙',
                '🐙🐙'
        ]
        ellDisplay == [
                '🐱',
                '🐱',
                '🐱🐱'
        ]
        essDisplay == [
                '🍫',
                '🍫🍫',
                '◽🍫'
        ]
    }
}
